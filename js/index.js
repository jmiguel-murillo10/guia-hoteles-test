/*archivo js test*/
$(function(){
    $("[data-toogle='tooltip'").tooltip();
    $("[data-toogle='popover'").popover();
    $('.carousel').carousel({
      interval:4000
    });
    $('#contacto').on('show.bs.modal', function(e){
      console.log('el modal contacto se está mostrando');
      /*Remove and Add Class of Contact Button */
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contacto').prop('disabled',true);

    });
    $('#contacto').on('shown.bs.modal', function(e){
      console.log('el modal contacto se mostró');
    });
    $('#contacto').on('hide.bs.modal', function(e){
      console.log('el modal contacto se oculta');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
      console.log('el modal contacto se ocultó');
       /*Remove and Add Class of Contact Button */
      $('#contactoBtn').removeClass('btn-primary');
      $('#contactoBtn').addClass('btn-outline-success');
      $('#contacto').prop('disabled',false);
    });
  });
